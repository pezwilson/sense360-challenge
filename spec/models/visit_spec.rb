require 'visit'

RSpec.describe Visit do
  context "with arrival after 8pm and departure before 8am" do
    it "returns the night visit total" do
      visit_time = Visit.night_minutes('5/20/2015 21:01:00','5/21/2015 06:01:00')
      expect(visit_time).to eq 540
    end
  end

  context "with arrival before 8pm and departure before 8am return only night hours after 8pm" do
    it "returns the night visit total" do
      visit_time = Visit.night_minutes('5/20/2015 19:01:00','5/21/2015 06:01:00')
      expect(visit_time).to eq 601
    end
  end

  context "with arrival before 8pm and departure after 8am return only between after 8pm and 8am" do
    it "returns the night visit total" do
      visit_time = Visit.night_minutes('5/20/2015 19:01:00','5/21/2015 10:01:00')
      expect(visit_time).to eq 719
    end
  end

  context "with arrival after 8am and departure before 8pm return 0" do
    it "returns the night visit total" do
      visit_time = Visit.night_minutes('5/21/2015 10:01:00','5/21/2015 19:01:00')
      expect(visit_time).to eq 0
    end
  end
end

