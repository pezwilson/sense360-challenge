require 'json'
require "./visit"
require "./location"

class HomeLocation

  def initialize
    file = File.read('visits.json')
    visits = JSON.parse(file)
    visits = visits['visits']
    visits.each do |visit|
      night_visit_time = Visit.night_minutes(visit['arrival_time_local'], visit['departure_time_local'])
      next if night_visit_time == 0

      lat = visit['latitude'].to_f.round(3).to_s
      lon = visit['longitude'].to_f.round(3).to_s

      location = Location.find_by_location(lat,lon)
      if location
        location.total_minutes += night_visit_time
      else
        location = Location.new
        location.latitude = lat
        location.longitude = lon
        location.total_minutes = night_visit_time
      end
      location.save
    end

    locations = Location.locations
    objects_only = []
    locations.each do |id, obj|
      objects_only.push obj
    end

    sorted_locations = objects_only.sort_by { |v| v.total_minutes }.reverse!
    longest_location = sorted_locations.first

    if longest_location.total_minutes >= 1800
      output = { latitude: longest_location.latitude, longitude: longest_location.longitude }
    else
      output = { error: 'unable to determine home location' }
    end
    output
  end

end

HomeLocation.new
