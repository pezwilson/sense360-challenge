class Location
  @@locations = {}
  attr_accessor :id, :latitude, :longitude, :total_minutes

  def self.locations
    @@locations
  end

  def self.find_by_location(lat, lon)
    @@locations.each do |id, location|
      return location if location.latitude == lat && location.longitude == lon
    end
    nil
  end

  def save
    if self.id.nil?
      self.id = random_number
    end
    @@locations[self.id] = self
  end

  private

  def random_number
    begin
      random = Random.new.bytes(8).bytes.join[0,8]
    end while !@@locations.empty? && @@locations[random]
    random
  end

end