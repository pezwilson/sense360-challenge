require 'date'

class Visit

  def self.night_minutes(arrival_time, departure_time)
    arrival = DateTime.strptime(arrival_time, "%m/%d/%Y %H:%M:%S")
    departure = DateTime.strptime(departure_time, "%m/%d/%Y %H:%M:%S")

    if arrival.hour < 20 && arrival.hour >= 8
      arrival = DateTime.new(arrival.year, arrival.month, arrival.day, 20, 0, 0)
    end

    if departure.hour < 20 && departure.hour >= 8
      departure = DateTime.new(departure.year, departure.month, departure.day, 7, 59, 59)
    end

    if departure > arrival
      total_minutes = departure - arrival
      return (total_minutes * 24 * 60).to_i
    end
    0
  end

end